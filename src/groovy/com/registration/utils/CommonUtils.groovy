/*
 * Copyright 2014
 * Created on : 18-09-14
 * Author     : Hardik 
 */
package com.registration.utils

/**
 * The Class CommonUtils.
 */
class CommonUtils {

	/**
	 * Validate email address.
	 *
	 * @param emailAddress the email address
	 * @param lang the lang
	 * @return the java.lang. object
	 */
	def static validateEmailAddress(def emailAddress, def lang){
		String domainEmailRegex, subDomainEmailRegex
		if(lang.equals('en')){
			domainEmailRegex = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.(?:[A-Z]{2,}|biz))+";
			subDomainEmailRegex = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.(?:[A-Z]{2,}|org|co)*(\\.(?:[a-z]{2,}|uk)*))+";
		}else if(lang.equals('br')){
			domainEmailRegex = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.(?:[A-Z]{2,}|info))+";
			subDomainEmailRegex = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.(?:[A-Z]{2,}|org)*(\\.(?:[a-z]{2,}|br)*))+";
		}else if(lang.equals('es')){
			domainEmailRegex = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.(?:[A-Z]{2,}|acrede-es|acrede))+";
		}

		Boolean subDomainresult = subDomainEmailRegex == null ? false : emailAddress.matches(subDomainEmailRegex);
		if (subDomainresult == false) {
			Boolean domainResult = emailAddress.matches(domainEmailRegex);
			if (domainResult == false) {
					return false
			}else{
				return true
			}
		}else{
			return true
		}
	}
}
