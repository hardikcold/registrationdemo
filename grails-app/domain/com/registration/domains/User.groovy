/*
 * Copyright 2014
 * Created on : 18-09-14
 * Author     : Hardik 
 */
package com.registration.domains

/**
 * The Class User.
 */
class User {

	/** The username. */
	String username
	
	/** The postal code. */
	String postalCode
	
	/** The email address. */
	String emailAddress
    
    /** The constraints. */
    static constraints = {
		username nullable : false, blank : false, unique : true, maxSize : 50
		postalCode nullable : false, blank : false, maxSize : 10
		emailAddress nullable : false, blank : false, unique : true, email: true, maxSize : 100
    }
}
