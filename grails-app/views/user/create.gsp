<%@ page import="com.registration.domains.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="register">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-user" class="content scaffold-create" role="main">
			
				<fieldset class="form">
					<div class="container">
            			<section>				
			                <div id="container_demo" >
			                    <div id="wrapper">
			                        <div id="login" class="animate form">
			                        	 <g:if test="${flash.message}">
											<div class="message" role="status">${flash.message}</div>
										</g:if>
			                            <g:form action="save" >
			                            	<h1> ${message(code: 'default.button.signup.label', default: 'Sign Up')} </h1> 
			                            	<g:render template="form"/>
			                                
			                                <p class="signin button"> 
												<input type="submit" value="${message(code: 'default.button.signup.label', default: 'Sign Up')}"/> 
											</p>
			                            </g:form>
			                        </div>
									
			                    </div>
			                </div>  
          			  </section>
       			 </div>
				</fieldset>
		</div>
	</body>
</html>
