<%@ page import="com.registration.domains.User" %>



<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="user.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" maxlength="50"  value="${userInstance?.username}"/>  <span style="color: red"><g:fieldError bean="${userInstance}" field="username" ></g:fieldError></span>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'postalCode', 'error')} required">
	<label for="postalCode">
		<g:message code="user.postalCode.label" default="Postal Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="postalCode" maxlength="10" value="${userInstance?.postalCode}"/> <span style="color: red"><g:fieldError bean="${userInstance}" field="postalCode" ></g:fieldError></span>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'emailAddress', 'error')} required">
	<label for="emailAddress">
		<g:message code="user.emailAddress.label" default="Email Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="emailAddress" maxlength="100"  value="${userInstance?.emailAddress}"/> <span style="color: red"><g:fieldError bean="${userInstance}" field="emailAddress" ></g:fieldError></span>
</div>

